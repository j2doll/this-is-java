﻿package sec02.exam01_path;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

public class PathExample {
	public static void main(String[] args) throws Exception {

		Path path = Paths.get("src/sec02/exam01_path/PathExample.java");
		System.out.println("[파일명] " + path.getFileName()); // [파일명] PathExample.java
		System.out.println("[부모 디렉토리명]: " + path.getParent().getFileName()); // [부모 디렉토리명]: exam01_path
		System.out.println("중첩 경로수: " + path.getNameCount()); // 중첩 경로수: 4
		
		System.out.println();
		for(int i=0 ; i< path.getNameCount() ; i++) {
			System.out.println( path.getName(i) );
		}
		/*
			src
			sec02
			exam01_path
			PathExample.java
		*/
		
		System.out.println();
		Iterator<Path> iterator = path.iterator();
		while( iterator.hasNext() ) {
			Path temp = iterator.next();
			System.out.println(temp.getFileName());
		}
		/*
			src
			sec02
			exam01_path
			PathExample.java
		 */

	}
}
